<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('resort_id');
            $table->string('name');
            $table->string('category')->nullable();
            $table->integer('amount')->nullable();
            $table->string('capacity')->nullable();
            $table->string('hours')->nullable();
            $table->string('size')->nullable();
            $table->string('feature')->nullable();
            $table->timestamps();
            $table->softDeletes();

            // add foreign key
//            $table->foreign('resort_id')
//                ->references('id')->on('resorts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rates');
    }
}
