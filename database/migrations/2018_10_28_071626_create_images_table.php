<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function (Blueprint $table) {
            $table->increments('id');
            $table->string('resort_id');
            $table->string('filename');
            $table->string('fileformat');
            $table->timestamps();
            $table->softDeletes();

            // add foreign key
//            $table->foreign('resort_id')
//                ->references('id')->on('resorts');
        });
        DB::statement("ALTER TABLE images ADD file  LONGBLOB");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images');
    }
}
