<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {

    $role = ['admin', 'owner'];

    return [
        'name' => $faker->name,
        'email' => 'admin@gmail.com',
        'role' => 'admin',
//        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'password' => '$2y$10$pcvkhEFWJpHt0Vv83y0NgO4vMjRr2Q6K3SOGP4JSuIMSZO9alPgMO', // 123123
        'remember_token' => str_random(10),
    ];
});
