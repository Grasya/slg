<?php

use Faker\Generator as Faker;

$factory->define(\App\Event::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence,
        'detail' => $faker->paragraph,
        'location' => $faker->address,
        'date' => $faker->dateTime,
    ];
});
