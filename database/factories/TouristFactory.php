<?php

use Faker\Generator as Faker;

$factory->define(App\Tourist::class, function (Faker $faker) {

    $category = ['Beach', 'Falls', 'Pools', 'Resort'];
    $location = ['Adecor', 'Anonang', 'Aumbay', 'Aundanao', 'Balet', 'Bandera', 'Caliclic (Dangca-an)', 'Camudmud', 'Catagman', 'Cawag'];

    return [
        'name' => $faker->sentence,
        'category' => $category[rand(0,3)],
        'location' => $location[rand(0,9)],
    ];
});
