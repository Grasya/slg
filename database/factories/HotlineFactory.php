<?php

use Faker\Generator as Faker;

$factory->define(App\Hotline::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'contact' => $faker->phoneNumber,
    ];
});
