<?php

use Illuminate\Database\Seeder;
use App\Hotline;

class HotlineSeeder extends Seeder
{
    /**
     * @var $hotline
     */
    private $hotline;

    /**
     * HotlineSeeder constructor.
     * @param Hotline $hotline
     */
    public function __construct( Hotline $hotline )
    {
        $this->hotline = $hotline;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $hotline = json_decode( file_get_contents( dirname( __DIR__ ) . '/data/hotline.json' ), true );

        $totalCount = count( $hotline );
        $savedCount = 0;

        $error = [];
        foreach ( $hotline as $data ) {

            $this->hotline->updateOrCreate(
                [ 'name' => trim(array_get( $data, 'name' )) ],
                [ 'contact' => trim(array_get( $data, 'contact' )) ]
            );

            ++$savedCount;
        }

        // file_put_contents(dirname( __DIR__ ) . '/data/not-saved-icd.json', json_encode($error));

        $errorCount = $totalCount - $savedCount;
        echo str_repeat( '-', 60 ) . "\n";
        echo "Actual items count: {$totalCount}\n";
        echo "Error count: {$errorCount}\n";
        echo "Added count: {$savedCount}\n";
        echo "Done seeding ...\n";
        echo str_repeat( '-', 60 ) . "\n";
    }
}
