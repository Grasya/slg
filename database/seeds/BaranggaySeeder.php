<?php

use Illuminate\Database\Seeder;
use App\Baranggay;

class BaranggaySeeder extends Seeder
{
    /**
     * @var $baranggay
     */
    private $baranggay;

    /**
     * BaranggaySeeder constructor.
     * @param Baranggay $baranggay
     */
    public function __construct( Baranggay $baranggay )
    {
        $this->baranggay = $baranggay;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $samal = json_decode( file_get_contents( dirname( __DIR__ ) . '/data/samal-barrangay.json' ), true );

        $totalCount = count( $samal );
        $savedCount = 0;

        $error = [];
        foreach ( $samal as $data ) {

            $this->baranggay->updateOrCreate(
                [ 'name' => trim(array_get( $data, 'name' )) ]
            );

            ++$savedCount;
        }

        // file_put_contents(dirname( __DIR__ ) . '/data/not-saved-icd.json', json_encode($error));

        $errorCount = $totalCount - $savedCount;
        echo str_repeat( '-', 60 ) . "\n";
        echo "Actual items count: {$totalCount}\n";
        echo "Error count: {$errorCount}\n";
        echo "Added count: {$savedCount}\n";
        echo "Done seeding ...\n";
        echo str_repeat( '-', 60 ) . "\n";
    }
}
