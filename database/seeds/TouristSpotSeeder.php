<?php

use Illuminate\Database\Seeder;

class TouristSpotSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Tourist::class, 50)->create();
        $totalCount = 50;
        $savedCount = 50;
        $errorCount = $totalCount - $savedCount;
        echo str_repeat( '-', 60 ) . "\n";
        echo "Actual items count: {$totalCount}\n";
        echo "Error count: {$errorCount}\n";
        echo "Added count: {$savedCount}\n";
        echo "Done seeding ...\n";
        echo str_repeat( '-', 60 ) . "\n";
    }
}
