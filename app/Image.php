<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Image extends Model
{
    use SoftDeletes;
    protected $softDelete = true;
    protected $fillable = [
        'filename',
        'fileformat',
        'resort_id',
        'file'
    ];

    public function resort()
    {
        return $this->belongsTo('App\Resort');
    }
}
