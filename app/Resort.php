<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Resort extends Model
{
    use SoftDeletes;
    protected $softDelete = true;
    protected $fillable = [
        'title',
        'user_id',
        'category',
        'amenity',
        'description',
        'location',
    ];

    public function image()
    {
        return $this->hasOne('App\Image');
    }
}
