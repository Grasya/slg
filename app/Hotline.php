<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Hotline extends Model
{
    use SoftDeletes;
    protected $softDelete = true;
    protected $fillable = [
        'name',
        'contact'
    ];
}
