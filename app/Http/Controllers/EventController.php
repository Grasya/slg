<?php

namespace App\Http\Controllers;

use App\Baranggay;
use App\Event;
use Illuminate\Http\Request;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $events = Event::query()
            ->get();

        $locations = Baranggay::query()
            ->get();

        return view('admin.event.index',
            [
                'events' => $events,
                'locations' => $locations
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('events.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'          => 'required',
            'location'      => 'required',
            'date'          => 'required',
            'detail'        => 'required'
        ]);

        Event::create([
            'name'          => $request->name,
            'location'      => $request->location,
            'date'          => $request->date,
            'detail'        => $request->detail,
        ]);

        session()->flash('message', 'Successfully Added!!');

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $detail = Event::find($id);
        return $detail;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $detail = Event::find($id);
        return $detail;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $event = Event::find($request->id);

        $event->name            = $request->name;
        $event->location        = $request->location;
        $event->date            = $request->date;
        $event->detail          = $request->detail;
        $event->save();

        session()->flash('message', 'Successfully Updated!!');

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $event = Event::find($request->id);
        $event->delete();

        session()->flash('message', 'Successfully Deleted!!');

        return back();
    }
}
