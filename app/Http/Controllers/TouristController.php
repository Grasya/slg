<?php

namespace App\Http\Controllers;

use App\Baranggay;
use App\Tourist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TouristController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tourists = Tourist::query()
            ->get();

        foreach ($tourists as $json) {
            $json['category'] = explode(',',$json['category']);
        }

        $locations = Baranggay::query()
            ->get();

        return view('admin.tourist.index',
            [
                'tourists' => $tourists,
                'locations' => $locations
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tourist.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = '';

        foreach ($request->category as $cat) {
            $category = $category.''.$cat.',';
        }

        $this->validate($request, [
            'name'          => 'required',
            'location'      => 'required',
            'category'      => 'required'
        ]);

        Tourist::create([
            'name'          => $request->name,
            'location'      => $request->location,
            'category'      => rtrim($category,',')
        ]);

        session()->flash('message', 'Successfully Added!!');

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $detail = Tourist::find($id);
        return $detail;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $detail = Tourist::find($id);
        return $detail;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $tourist = Tourist::find($request->id);

        $category = '';

        foreach ($request->category as $cat) {
            $category = $category.''.$cat.',';
        }

        $tourist->name            = $request->name;
        $tourist->location        = $request->location;
        $tourist->category        = rtrim($category,',');
        $tourist->save();

        session()->flash('message', 'Successfully Updated!!');

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $event = Tourist::find($request->id);
        $event->delete();

        session()->flash('message', 'Successfully Deleted!!');

        return back();
    }
}
