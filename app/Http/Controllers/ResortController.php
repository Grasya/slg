<?php

namespace App\Http\Controllers;

use App\Baranggay;
use App\Image;
use App\Rate;
use App\Resort;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ResortController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $resorts = Resort::query()
            ->get();

        $locations = Baranggay::query()
            ->get();

        return view('admin.resort.index',
            [
                'resorts' => $resorts,
                'locations' => $locations
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = Resort::query()
            ->where('user_id','=', Auth::user()->id)
            ->first();

        $this->validate($request, [
            'title'          => 'required',
            'contact'       => 'required',
            'location'      => 'required',
            'category'      => 'required',
            'description'   => 'required',
        ]);

        Resort::create([
            'title'          => $request->title,
            'user_id'       => Auth::user()->id,
            'contact'       => $request->contact,
            'location'      => $request->location,
            'category'      => $request->category,
            'description'   => $request->description,
        ]);

        session()->flash('message', 'Successfully Added!!');

        return redirect(route('owner.index'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $resort = Resort::find($id);

        return view('admin.view.show',
            [
                'view' => $resort
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->edit == 'title') {
            Resort::find($id)->update(['title' => $request->title]);

            session()->flash('message', 'Successfully Update!!');
        }

        if ($request->edit == 'description') {
            Resort::find($id)->update(['description' => $request->description]);

            session()->flash('message', 'Successfully Update!!');
        }

        if ($request->edit == 'ccl') {
            Resort::find($id)->update(
                [
                    'contact'   => $request->contact,
                    'category'  => $request->category,
                    'location'  => $request->location,
                ]
            );

            session()->flash('message', 'Successfully Update!!');
        }

        return redirect(route('owner.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
