<?php

namespace App\Http\Controllers;

use App\Rate;
use App\Resort;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rates = Rate::query()
            ->where('resort_id','=',Auth::user()->id)
            ->paginate(10);

        return view('owner.dashboard.show', ['rates' => $rates]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = Resort::query()
            ->where('user_id','=', Auth::user()->id)
            ->first();
        $this->validate($request, [
            'name'          => 'required',
            'category'      => 'required'
        ]);

        Rate::create([
            'name'          => $request->name,
            'resort_id'     => $id['id'],
            'category'      => $request->category,
            'amount'        => $request->amount,
            'size'          => $request->size,
            'capacity'      => $request->capacity,
            'hours'         => $request->hours,
            'feature'       => $request->feature
        ]);

        session()->flash('message', 'Successfully Added!!');

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $rate = Rate::find($id);

        $rate->name          = $request->name;
        $rate->category      = $request->category;
        $rate->amount        = $request->amount;
        $rate->size          = $request->size;
        $rate->capacity      = $request->capacity;
        $rate->hours         = $request->hours;
        $rate->feature       = $request->feature;

        $rate->save();

        session()->flash('message', 'Successfully Update!!');

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rate = Rate::find($id);
        $rate->delete();

        session()->flash('message', 'Successfully Update!!');

        return back();
    }
}
