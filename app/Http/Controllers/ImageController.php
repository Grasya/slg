<?php

namespace App\Http\Controllers;

use App\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'images' => 'required',
            'images.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:5048'
        ]);
        $images = $request->file('images');

        // Get the contents of the file


        if (!empty($images)) {
            foreach ($images as $image) {
//                $extension = $image->getClientOriginalExtension();
                $contents = $image->openFile()->fread($image->getSize());
                Storage::disk('uploads')->put($image->getClientOriginalName(),  File::get($image));

                Image::create([
                    'filename'         => $image->getClientOriginalName(),
                    'fileformat'         => $image->getClientMimeType(),
                    'resort_id'        => $request->resort_id,
                    'file'             => $contents
                ]);
            }
        }

        session()->flash('message', 'Successfully Added!!');

        return redirect(route('owner.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function show(Image $image)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function edit(Image $image)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Image $image)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Image $image)
    {
        $img = Image::find($id);
        $img->delete();

        return back();
    }
}
