<?php

namespace App\Http\Controllers;

use App\Amenity;
use App\Baranggay;
use App\Image;
use App\Rate;
use App\Resort;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OwnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::user()->id;

        $resort = Resort::query()
            ->where('user_id','=', $user_id)
            ->get();


        foreach ($resort as $json) {
            $json['category'] = explode(',',$json['category']);
        }

        foreach ($resort as $json) {
            $json['amenity'] = explode(',',$json['amenity']);
        }

        $locations = Baranggay::all();



        return view('owner.dashboard.index',
            [
                'resort' => json_decode($resort) ? $resort : '',
                'view' => $resort ? $resort : '',
                'locations' => $locations ? $locations : '',
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id = Auth::user()->id;

        $this->validate($request, [
            'title'         => 'required',
            'location'      => 'required',
            'category'      => 'required'
        ]);

        $category = '';

        foreach ($request->category as $cat) {
            $category = $category.''.$cat.',';
        }

        $insert = Resort::create([
            'title'         => $request->title,
            'user_id'       => $user_id,
            'description'   => $request->description,
            'location'      => $request->location,
            'category'      => rtrim($category,','),
            'amenity'       => ''
        ]);

        session()->flash('message', 'Successfully Added!!');

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $user_id = Auth::user()->id;
        $resort = Resort::find($request->id);
        $category = '';
        $amenity = '';
//        dd($request->id);die;
        foreach ($request->category as $cat) {
            $category = $category.''.$cat.',';
        }
        foreach ($request->amenity as $ame) {
            $amenity = $amenity.''.$ame.',';
        }
        $resort->title              = $request->title;
        $resort->location           = $request->location;
        $resort->category           = rtrim($category,',');
        $resort->amenity            = rtrim($amenity,',');
        $resort->description        = $request->description;
        $resort->save();

        session()->flash('message', 'Successfully Updated!!');

        return redirect(route('owner.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $detail = Resort::find($id);
        return $detail;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $resort = Resort::find($request->id);
        $category = '';
        $amenity = '';
//        dd($request->amenity);die;
        foreach ($request->category as $cat) {
            $category = $category.''.$cat.',';
        }
        foreach ($request->$amenity as $ame) {
            $amenity = $amenity.''.$ame.',';
        }

        $resort->title              = $request->title;
        $resort->location           = $request->location;
        $resort->category           = rtrim($category,',');
        $resort->amenity            = rtrim($amenity,',');
        $resort->description        = $request->description;
        $resort->save();

        session()->flash('message', 'Successfully Updated!!');

        return redirect(route('owner.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
