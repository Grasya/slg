<?php

namespace App\Http\Controllers;

use App\Event;
use App\Hotline;
use App\Resort;
use App\Tourist;
use App\User;
use Illuminate\Http\Request;

class ArchiveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hotline = Hotline::query()
            ->whereNotNull('deleted_at')
            ->get();

        $resort = Resort::query()
            ->whereNotNull('deleted_at')
            ->get();

        $tourist = Tourist::query()
            ->whereNotNull('deleted_at')
            ->get();

        $event = Event::query()
            ->whereNotNull('deleted_at')
            ->get();

//        $user = User::query()
//            ->where('deleted_at','!=','')
//            ->get();

        return view('admin.archive.index',
            [
                'hotlines'  => $hotline,
                'resorts'   => $resort,
                'events'    => $event,
                'tourists'   => $tourist
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
