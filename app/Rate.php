<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rate extends Model
{
    use SoftDeletes;
    protected $softDelete = true;
    protected $fillable = [
        'name',
        'resort_id',
        'category',
        'amount',
        'capacity',
        'hours',
        'size',
        'feature',
    ];
}
