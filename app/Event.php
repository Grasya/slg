<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model
{
    use SoftDeletes;
    protected $softDelete = true;
    protected $fillable = [
        'name',
        'detail',
        'location',
        'date',
    ];
}
