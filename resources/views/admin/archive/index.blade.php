@extends('admin.layout.app')

@section('content')

    <!-- ALERT-->
    @if (session()->has('message'))
        <div class="alert au-alert-success alert-dismissible fade show au-alert au-alert--70per" role="alert">
            <i class="zmdi zmdi-check-circle"></i>
            <span class="content">{{ session()->get('message') }}</span>
            <button class="close" type="button" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">
                <i class="zmdi zmdi-close-circle"></i>
            </span>
            </button>
        </div>
    @endif
    <!-- END ALERT-->

    <!-- DATA TABLE-->
    <section class="p-t-20">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Archives</h4>
                        </div>
                        <div class="card-body">
                            <div class="default-tab">
                                <nav>
                                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                        <a class="nav-item nav-link active show" id="nav-event-tab" data-toggle="tab" href="#nav-event" role="tab" aria-controls="nav-event" aria-selected="false">Events Archive</a>
                                        <a class="nav-item nav-link" id="nav-tourist-tab" data-toggle="tab" href="#nav-tourist" role="tab" aria-controls="nav-tourist" aria-selected="true">Tourist Spot Archive</a>
                                        <a class="nav-item nav-link" id="nav-hotline-tab" data-toggle="tab" href="#nav-hotline" role="tab" aria-controls="nav-hotline" aria-selected="false">Hotline Archive</a>
                                        <a class="nav-item nav-link" id="nav-resort-tab" data-toggle="tab" href="#nav-resort" role="tab" aria-controls="nav-resort" aria-selected="false">Resort Archive</a>
                                    </div>
                                </nav>
                                <div class="tab-content pl-3 pt-2" id="nav-tabContent">
                                    <div class="tab-pane fade active show" id="nav-event" role="tabpanel" aria-labelledby="nav-event-tab">
                                        <div class="top-campaign">
                                            <h3 class="title-3 m-b-30">Event</h3>
                                            <div class="table-responsive">
                                                <table class="table table-top-campaign">
                                                    <thead>
                                                    <tr>
                                                        <th>Event Name</th>
                                                        <th>Location</th>
                                                        <th>Date</th>
                                                        <th></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach ($events as $event)
                                                        <tr class="tr-shadow">
                                                            <td class="title-5">{{ $event->name }}</td>
                                                            <td>
                                                                <span class="block-email">{{ $event->location }}</span>
                                                            </td>
                                                            <td>{{ date('M j, Y ',strtotime($event->date)) }}</td>
                                                            <td>
                                                                <div class="table-data-feature">
                                                                    <button class="item" data-toggle="tooltip" data-placement="top" title="View Details" onclick="view({{ $event->id }})">
                                                                        <i class="zmdi zmdi-more"></i>
                                                                    </button>
                                                                    <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                                                        <i class="zmdi zmdi-edit"></i>
                                                                    </button>
                                                                    <button class="item" data-toggle="tooltip" data-placement="top" title="Remove">
                                                                        <i class="zmdi zmdi-delete"></i>
                                                                    </button>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="nav-tourist" role="tabpanel" aria-labelledby="nav-tourist-tab">
                                        <div class="top-campaign">
                                            <h3 class="title-3 m-b-30">Tourist Spot</h3>
                                            <div class="table-responsive">
                                                <table class="table table-top-campaign">
                                                    <thead>
                                                    <tr>
                                                        <th>Event Name</th>
                                                        <th>Location</th>
                                                        <th>Category</th>
                                                        <th></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach ($tourists as $tourist)
                                                        <tr class="tr-shadow">
                                                            <td class="title-5">{{ $tourist->name }}</td>
                                                            <td class="title-5">{{ $tourist->location }}</td>
                                                            <td class="title-5">
                                                                @foreach (\GuzzleHttp\json_decode($tourist->category) as $cat)
                                                                    @if ($cat == 'Falls')
                                                                        <span class="role member">
                                                                            {{ $cat }}
                                                                        </span>
                                                                    @elseif ($cat == 'Pools')
                                                                        <span class="role user">
                                                                            {{ $cat }}
                                                                        </span>
                                                                    @elseif ($cat == 'Beach')
                                                                        <span class="role superadmin">
                                                                            {{ $cat }}
                                                                        </span>
                                                                    @elseif ($cat == 'Resort')
                                                                        <span class="role admin">
                                                                            {{ $cat }}
                                                                        </span>
                                                                    @endif
                                                                @endforeach
                                                            </td>
                                                            <td>
                                                                <div class="table-data-feature">
                                                                    <button class="item" data-toggle="tooltip" data-placement="top" title="Edit" onclick="edit({{ $tourist->id }})">
                                                                        <i class="zmdi zmdi-edit"></i>
                                                                    </button>
                                                                    <button class="item" data-toggle="tooltip" data-placement="top" title="Remove" onclick="del({{ $tourist->id }})">
                                                                        <i class="zmdi zmdi-delete"></i>
                                                                    </button>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="nav-hotline" role="tabpanel" aria-labelledby="nav-hotline-tab">
                                        <div class="top-campaign">
                                            <h3 class="title-3 m-b-30">Hotline</h3>
                                            <div class="table-responsive">
                                                <table class="table table-top-campaign">
                                                    <thead>
                                                    <tr>
                                                        <th>Hotline</th>
                                                        <th>Contact Number</th>
                                                        <th>Created Date</th>
                                                        <th>Updated Date</th>
                                                        <th></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach ($hotlines as $hotline)
                                                        <tr class="tr-shadow">
                                                            <td class="title-5">{{ $hotline->name }}</td>
                                                            <td>
                                                                <span class="block-email">{{ $hotline->contact }}</span>
                                                            </td>
                                                            <td>{{ date('h:i:s A | M j, Y ',strtotime($hotline->created_at)) }}</td>
                                                            <td>{{ date('h:i:s A | M j, Y ',strtotime($hotline->updated_at)) }}</td>
                                                            <td>
                                                                <div class="table-data-feature">
                                                                    <button class="item" data-toggle="tooltip" data-placement="top" title="Edit" onclick="edit({{ $hotline->id }})">
                                                                        <i class="zmdi zmdi-edit"></i>
                                                                    </button>
                                                                    <button class="item" data-toggle="tooltip" data-placement="top" title="Remove" onclick="del({{ $hotline->id }})">
                                                                        <i class="zmdi zmdi-delete"></i>
                                                                    </button>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="nav-resort" role="tabpanel" aria-labelledby="nav-resort-tab">
                                        <div class="top-campaign">
                                            <h3 class="title-3 m-b-30">Resort</h3>
                                            <div class="table-responsive">
                                                <table class="table table-top-campaign">
                                                    <thead>
                                                    <tr>
                                                        <th>Resort Name</th>
                                                        <th>Location</th>
                                                        <th>Created Date</th>
                                                        <th>Updated Date</th>
                                                        <th></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach ($resorts as $resort)
                                                        <tr class="tr-shadow">
                                                            <td class="title-5">{{ $resort->name }}</td>
                                                            <td>
                                                                <span class="block-email">{{ $resort->location }}</span>
                                                            </td>
                                                            <td>{{ date('h:i:s A | M j, Y ',strtotime($resort->created_at)) }}</td>
                                                            <td>{{ date('h:i:s A | M j, Y ',strtotime($resort->updated_at)) }}</td>
                                                            <td>
                                                                <div class="table-data-feature">
                                                                    <button class="item" data-toggle="tooltip" data-placement="top" title="Visit Site">
                                                                        <i class="zmdi zmdi-eye"></i>
                                                                    </button>
                                                                    {{--<button class="item" data-toggle="tooltip" data-placement="top" title="Edit">--}}
                                                                    {{--<i class="zmdi zmdi-edit"></i>--}}
                                                                    {{--</button>--}}
                                                                    <button class="item" data-toggle="tooltip" data-placement="top" title="Remove">
                                                                        <i class="zmdi zmdi-delete"></i>
                                                                    </button>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END DATA TABLE-->

@endsection

@section('javascript')
    <script>
        $(document).ready( function () {
            $('.table').DataTable();

            $('#detail-modal').on('click', function () {
                $('#detailmodal').modal('show');
            });

            $('#add-modal').on('click', function () {
                $('#addmodal').modal('show');
            });
        } );

        function modalField(modal,data) {
            modal.find('#id').val(data['id']);
            modal.find('#name').val(data['name']);
            modal.find('#intTextBox').val(data['contact']);
        }

        function clearModalField(modal) {
            modal.on('hidden.bs.modal', function () {
                modal.find('#id').empty();
                modal.find('#name').empty();
                modal.find('#intTextBox').empty();
            })
        }

        function view(id) {
            // alert(id);
            let modal = $('#view-modal');
            $.ajax({
                type: 'get',
                url: 'hotline/'+id,
                success: function (data) {
                    console.log(data);
                    // alert(data['location']);
                    modalField(modal,data);
                }
            });
            modal.modal('show');
            clearModalField(modal);
        }

        function del(id) {
            let modal = $('#delete-modal');
            $.ajax({
                type: 'get',
                url: 'hotline/'+id,
                success: function (data) {
                    modalField(modal,data);
                }
            });
            modal.modal('show');
            clearModalField(modal);
        }

        function edit(id) {
            // alert(id);
            let modal = $('#edit-modal');
            $.ajax({
                type: 'get',
                url: 'hotline/'+id,
                success: function (data) {
                    console.log(data);
                    // alert(data['location']);
                    modalField(modal,data);
                }
            });
            modal.modal('show');
            clearModalField(modal);
        }
    </script>
@stop
