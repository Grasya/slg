@extends('admin.layout.app')

@section('content')

    <!-- ALERT-->
    @if (session()->has('message'))
        <div class="alert au-alert-success alert-dismissible fade show au-alert au-alert--70per" role="alert">
            <i class="zmdi zmdi-check-circle"></i>
            <span class="content">{{ session()->get('message') }}</span>
            <button class="close" type="button" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">
                <i class="zmdi zmdi-close-circle"></i>
            </span>
            </button>
        </div>
    @endif
    <!-- END ALERT-->

    <!-- WELCOME-->
    <section class="welcome p-t-10">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="title-4">Resorts List
                        {{--<span>{{ Auth::user()->name }}</span>--}}
                    </h1>
                    <hr class="line-seprate">
                </div>
            </div>
        </div>
    </section>
    <!-- END WELCOME-->

    <!-- DATA TABLE-->
    <section class="p-t-20">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    {{-- Resort List --}}
                    <div class="table-responsive table-responsive-data2">
                        <table class="table table-data2 table-striped table-bordered" id="eventTable">
                            <thead>
                            <tr>
                                <th>Resort Name</th>
                                <th>Location</th>
                                <th>Created Date</th>
                                <th>Updated Date</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($resorts as $resort)
                                <tr class="tr-shadow">
                                    <td class="title-5">{{ $resort->title }}</td>
                                    <td>
                                        <span class="block-email">{{ $resort->location }}</span>
                                    </td>
                                    <td>{{ date('h:i:s A | M j, Y ',strtotime($resort->created_at)) }}</td>
                                    <td>{{ date('h:i:s A | M j, Y ',strtotime($resort->updated_at)) }}</td>
                                    <td>
                                        <div class="table-data-feature">
                                            <a href="{{ route('view.index') }}" class="item" data-toggle="tooltip" data-placement="top" title="Visit Site">
                                                <i class="zmdi zmdi-eye"></i>
                                            </a>
                                            <button class="item" data-toggle="tooltip" data-placement="top" title="Remove">
                                                <i class="zmdi zmdi-delete"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END DATA TABLE-->

    <!-- add modal -->
    <div class="modal fade" id="addmodal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="scrollmodalLabel">Event Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('event.store') }}" method="post" class="form-horizontal">
                    <div class="modal-body">
                        {{--CONTENT--}}
                        @csrf
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label for="name" class=" form-control-label">Event Name</label>
                            </div>
                            <div class="col-12 col-md-9">
                                <input type="text" id="name" name="name" placeholder="Event Name" class="form-control" required>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label for="location" class=" form-control-label">Location</label>
                            </div>
                            <div class="col-12 col-md-9">
                                <select class="form-control" name="location" required>
                                    <option value="">Select Location</option>
                                    @foreach ($locations as $location)
                                        <option value="{{ $location->name }}">{{ $location->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label for="date" class=" form-control-label">Dates</label>
                            </div>
                            <div class="col-12 col-md-9">
                                <input type="date" id="date" name="date" placeholder="Event Date" class="form-control" required>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label for="detail" class=" form-control-label">History / Details</label>
                            </div>
                            <div class="col-12 col-md-9">
                                <textarea name="detail" id="detail" rows="9" placeholder="Content..." class="form-control" required></textarea>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-danger btn-sm" data-dismiss="modal">
                            <i class="fa fa-ban"></i> Close
                        </button>
                        <button type="submit" class="btn btn-primary btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Submit
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- end modal scroll -->

@endsection

@section('javascript')
    <script>
        $(document).ready( function () {
            $('#location').select2();
            $('#eventTable').DataTable();

            $('#detail-modal').on('click', function () {
                $('#detailmodal').modal('show');
            });

            $('#add-modal').on('click', function () {
                $('#addmodal').modal('show');
            });

        } );
    </script>
@stop
