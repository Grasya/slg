@extends('admin.layout.app')

@section('content')

    <!-- ALERT-->
    @if (session()->has('message'))
        <div class="alert au-alert-success alert-dismissible fade show au-alert au-alert--70per" role="alert">
            <i class="zmdi zmdi-check-circle"></i>
            <span class="content">{{ session()->get('message') }}</span>
            <button class="close" type="button" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">
                <i class="zmdi zmdi-close-circle"></i>
            </span>
            </button>
        </div>
    @endif
    <!-- END ALERT-->

    <!-- WELCOME-->
    <section class="welcome p-t-10">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="title-4">Tourist Spot List
                        {{--<span>{{ Auth::user()->name }}</span>--}}
                    </h1>
                    <hr class="line-seprate">
                </div>
            </div>
        </div>
    </section>
    <!-- END WELCOME-->

    <!-- DATA TABLE-->
    <section class="p-t-20">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    {{--<h3 class="title-5 m-b-35">Events</h3>--}}
                    <div class="table-data__tool">
                        <div class="table-data__tool-right">
                            <button class="au-btn au-btn-icon au-btn--green au-btn--small" id="add-modal">
                                <i class="zmdi zmdi-plus"></i>Tourist Spot
                            </button>
                        </div>
                    </div>
                    {{-- User accounts --}}
                    <div class="table-responsive table-responsive-data2">
                        <table class="table table-data2 table-striped table-bordered table-hover" id="eventTable">
                            <thead>
                            <tr>
                                <th>Tourist Spots</th>
                                <th>Location</th>
                                <th>Category</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach ($tourists as $tourist)
                                <tr class="tr-shadow">
                                    <td class="title-5">{{ $tourist->name }}</td>
                                    <td class="title-5">{{ $tourist->location }}</td>
                                    <td class="title-5">
                                        @foreach($tourist->category as $category)
                                            @if ($category == 'Cave')
                                                <span class="role member">
                                                    {{ $category }}
                                                </span>
                                            @elseif ($category == 'Mountain')
                                                <span class="role user">
                                                    {{ $category }}
                                                </span>
                                            @elseif ($category == 'Diving')
                                                <span class="role superadmin">
                                                    {{ $category }}
                                                </span>
                                            @elseif ($category == 'Falls')
                                                <span class="role admin">
                                                    {{ $category }}
                                                </span>
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>
                                        <div class="table-data-feature">
                                            <button class="item" data-toggle="tooltip" data-placement="top" title="Edit" onclick="edit({{ $tourist->id }})">
                                                <i class="zmdi zmdi-edit"></i>
                                            </button>
                                            <button class="item" data-toggle="tooltip" data-placement="top" title="Remove" onclick="del({{ $tourist->id }})">
                                                <i class="zmdi zmdi-delete"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END DATA TABLE-->

    <!-- add modal -->
    <div class="modal fade" id="addmodal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="scrollmodalLabel">Event Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('tourist.store') }}" method="post" class="form-horizontal">
                    <div class="modal-body">
                        {{--CONTENT--}}
                        @csrf
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label for="name" class=" form-control-label">Tourist Spot Name</label>
                            </div>
                            <div class="col-12 col-md-9">
                                <input type="text" id="name" name="name" placeholder="Tourist Spot Name" class="form-control" required>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label for="location" class=" form-control-label">Location</label>
                            </div>
                            <div class="col-12 col-md-9">
                                <select class="form-control" name="location" required>
                                    <option value="">Select Location</option>
                                    @foreach ($locations as $location)
                                        <option value="{{ $location->name }}">{{ $location->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label for="category" class=" form-control-label">Category</label>
                            </div>
                            <div class="col-12 col-md-3">
                                <div class="checkbox">
                                    <label for="cat1" class="form-check-label ">
                                        <input type="checkbox" id="cat1" name="category[]" value="Cave" class="form-check-input"> Cave
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label for="cat2" class="form-check-label ">
                                        <input type="checkbox" id="cat2" name="category[]" value="Mountain" class="form-check-input"> Mountain
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label for="cat3" class="form-check-label ">
                                        <input type="checkbox" id="cat3" name="category[]" value="Diving" class="form-check-input"> Diving
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label for="cat4" class="form-check-label ">
                                        <input type="checkbox" id="cat4" name="category[]" value="Falls" class="form-check-input"> Falls
                                    </label>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-danger btn-sm" data-dismiss="modal">
                            <i class="fa fa-ban"></i> Close
                        </button>
                        <button type="submit" class="btn btn-primary btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Submit
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- end add modal -->

    <!-- edit modal -->
    <div class="modal fade" id="editmodal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="scrollmodalLabel">Event Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('tourist.update', 0) }}" method="post" class="form-horizontal">
                    <div class="modal-body">
                        {{--CONTENT--}}
                        @csrf
                        @method('put')
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label for="name" class=" form-control-label">Tourist Spot Name</label>
                            </div>
                            <div class="col-12 col-md-9">
                                <input type="hidden" id="id" name="id">
                                <input type="text" id="name" name="name" placeholder="Tourist Spot Name" class="form-control" required>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label for="location" class=" form-control-label">Location</label>
                            </div>
                            <div class="col-12 col-md-9">
                                <select class="form-control" name="location" id="location" required>
                                    <option value="">Select Location</option>
                                    @foreach ($locations as $location)
                                        <option value="{{ $location->name }}">{{ $location->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label for="category" class=" form-control-label">Category</label>
                            </div>
                            <div class="col-12 col-md-3">
                                <div class="checkbox">
                                    <label for="category1" class="form-check-label ">
                                        <input type="checkbox" id="category1" name="category[]" value="Cave" class="form-check-input"> Cave
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label for="category2" class="form-check-label ">
                                        <input type="checkbox" id="category2" name="category[]" value="Mountain" class="form-check-input"> Mountain
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label for="category3" class="form-check-label ">
                                        <input type="checkbox" id="category3" name="category[]" value="Diving" class="form-check-input"> Diving
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label for="category4" class="form-check-label ">
                                            <input type="checkbox" id="category4" name="category[]" value="Falls" class="form-check-input"> Falls
                                    </label>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-danger btn-sm" data-dismiss="modal">
                            <i class="fa fa-ban"></i> Close
                        </button>
                        <button type="submit" class="btn btn-primary btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Save Changes
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- end edit modal -->

    <!-- delete modal -->
    <div class="modal fade" id="deletemodal" tabindex="-1" role="dialog" aria-labelledby="staticModalLabel" aria-hidden="true"
         data-backdrop="static">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticModalLabel">Remove Tourist Spot</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="d-inline-block" method="post" action="{{ route('tourist.destroy', 0) }}">
                    @csrf
                    @method('delete')
                    <div class="modal-body">
                        <input type="hidden" id="id" name="id">
                        <input type="text" class="form-control" id="name" disabled>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">
                            <i class="fa fa-ban"></i> Cancel
                        </button>
                        <button type="submit" class="btn btn-danger btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Delete
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- end delete modal -->

@endsection

@section('javascript')
    <script>
        $(document).ready( function () {
            $('#eventTable').DataTable();

            $('#detail-modal').on('click', function () {
                $('#detailmodal').modal('show');
            });

            $('#add-modal').on('click', function () {
                $('#addmodal').modal('show');
            });
        } );

        function modalField(modal, data) {

            modal.find('#id').val(data['id']);
            modal.find('#name').val(data['name']);
            modal.find('#location').val(data['location']);
            let category = data['category'].split(',');
            console.log(category);
            for(let i=0; i<category.length; i++) {
                if(category[i] === 'Cave') {
                    modal.find('#category1').prop('checked', true);
                }
                if(category[i] === 'Mountain') {
                    modal.find('#category2').prop('checked', true);
                }
                if(category[i] === 'Diving') {
                    modal.find('#category3').prop('checked', true);
                }
                if(category[i] === 'Falls') {
                    modal.find('#category4').prop('checked', true);
                }
            }
        }

        function clearModalField(modal) {
            modal.on('hidden.bs.modal', function () {
                modal.find('#id').empty();
                modal.find('#name').empty();
                // modal.find('#location').empty();
                modal.find('#category1').prop('checked', false);
                modal.find('#category2').prop('checked', false);
                modal.find('#category3').prop('checked', false);
                modal.find('#category4').prop('checked', false);
            })
        }

        function view(id) {
            // alert(id);
            let modal = $('#view-modal');
            $.ajax({
                type: 'get',
                url: 'tourist/'+id,
                success: function (data) {
                    console.log(data);
                    // alert(data['location']);
                    modalField(modal,data);
                }
            });
            modal.modal('show');
            clearModalField(modal);
        }

        function del(id) {
            let modal = $('#deletemodal');
            $.ajax({
                type: 'get',
                url: 'tourist/'+id,
                success: function (data) {
                    modalField(modal,data);
                }
            });
            modal.modal('show');
            clearModalField(modal);
        }

        function edit(id) {
            // alert(id);
            let modal = $('#editmodal');
            $.ajax({
                type: 'get',
                url: 'tourist/'+id,
                success: function (data) {
                    console.log(data);
                    // alert(data['location']);
                    modalField(modal,data);
                }
            });
            modal.modal('show');
            clearModalField(modal);
        }
    </script>
@stop
