@extends('admin.layout.app')

@section('content')

    <!-- ALERT-->
    @if (session()->has('message'))
        <div class="alert au-alert-success alert-dismissible fade show au-alert au-alert--70per" role="alert">
            <i class="zmdi zmdi-check-circle"></i>
            <span class="content">{{ session()->get('message') }}</span>
            <button class="close" type="button" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">
                <i class="zmdi zmdi-close-circle"></i>
            </span>
            </button>
        </div>
    @endif
    <!-- END ALERT-->

    <!-- WELCOME-->
    <section class="welcome p-t-10">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="title-4">Hotline List
                        {{--<span>{{ Auth::user()->name }}</span>--}}
                    </h1>
                    <hr class="line-seprate">
                </div>
            </div>
        </div>
    </section>
    <!-- END WELCOME-->

    <!-- DATA TABLE-->
    <section class="p-t-20">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-data__tool">
                        <div class="table-data__tool-right">
                            <button class="au-btn au-btn-icon au-btn--green au-btn--small" id="add-modal">
                                <i class="zmdi zmdi-plus"></i>Hotline
                            </button>
                        </div>
                    </div>
                    {{-- Hotline List --}}
                    <div class="table-responsive table-responsive-data2">
                        <table class="table table-data2 table-striped table-bordered" id="eventTable">
                            <thead>
                            <tr>
                                <th>Hotline</th>
                                <th>Contact Number</th>
                                <th>Created Date</th>
                                <th>Updated Date</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($hotlines as $hotline)
                                <tr class="tr-shadow">
                                    <td class="title-5">{{ $hotline->name }}</td>
                                    <td>
                                        <span class="block-email">{{ $hotline->contact }}</span>
                                    </td>
                                    <td>{{ date('h:i:s A | M j, Y ',strtotime($hotline->created_at)) }}</td>
                                    <td>{{ date('h:i:s A | M j, Y ',strtotime($hotline->updated_at)) }}</td>
                                    <td>
                                        <div class="table-data-feature">
                                            <button class="item" data-toggle="tooltip" data-placement="top" title="Edit" onclick="edit({{ $hotline->id }})">
                                            <i class="zmdi zmdi-edit"></i>
                                            </button>
                                            <button class="item" data-toggle="tooltip" data-placement="top" title="Remove" onclick="del({{ $hotline->id }})">
                                                <i class="zmdi zmdi-delete"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END DATA TABLE-->

    <!-- add modal -->
    <div class="modal fade" id="addmodal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="scrollmodalLabel">Event Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('hotline.store') }}" method="post" class="form-horizontal">
                    <div class="modal-body">
                        {{--CONTENT--}}
                        @csrf
                        <div class="row form-group">
                            <div class="col col-md-12">
                                <label for="name" class=" form-control-label">Hotline Name</label>
                            </div>
                            <div class="col-12 col-md-12">
                                <input type="text" id="name" name="name" placeholder="Hotline Name" class="form-control" required>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-12">
                                <label for="intTextBox" class=" form-control-label">Contact Number</label>
                            </div>
                            <div class="col-12 col-md-12">
                                <input id="intTextBox" name="contact" placeholder="Contact Number" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-danger btn-sm" data-dismiss="modal">
                            <i class="fa fa-ban"></i> Close
                        </button>
                        <button type="submit" class="btn btn-primary btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Submit
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- end modal scroll -->

    <!-- edit modal -->
    <div class="modal fade" id="edit-modal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="scrollmodalLabel">Event Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" action="{{ route('hotline.update', 0) }}" class="form-horizontal">
                    <div class="modal-body">
                        {{--CONTENT--}}
                        @csrf
                        @method('put')
                        <div class="row form-group">
                            <div class="col col-md-12">
                                <label for="name" class=" form-control-label">Hotline Name</label>
                            </div>
                            <div class="col-12 col-md-12">
                                <input type="hidden" id="id" name="id">
                                <input type="text" id="name" name="name" placeholder="Hotline Name" class="form-control" required>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-12">
                                <label for="intTextBox" class=" form-control-label">Contact Number</label>
                            </div>
                            <div class="col-12 col-md-12">
                                <input id="intTextBox" name="contact" placeholder="Contact Number" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-danger btn-sm" data-dismiss="modal">
                            <i class="fa fa-ban"></i> Close
                        </button>
                        <button type="submit" class="btn btn-primary btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Submit
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- end edit modal scroll -->

    <!-- delete modal -->
    <div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="staticModalLabel" aria-hidden="true"
         data-backdrop="static">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticModalLabel">Remove Hotline</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="d-inline-block" method="post" action="{{ route('hotline.destroy', 0) }}">
                    @csrf
                    @method('delete')
                    <div class="modal-body">
                        <input type="hidden" id="id" name="id">
                        <input type="text" class="form-control" id="name" disabled>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">
                            <i class="fa fa-ban"></i> Cancel
                        </button>
                        <button type="submit" class="btn btn-danger btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Delete
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- end delete modal -->

@endsection

@section('javascript')
    <script>
        $(document).ready( function () {
            $('#location').select2();
            $('#eventTable').DataTable();

            $('#detail-modal').on('click', function () {
                $('#detailmodal').modal('show');
            });

            $('#add-modal').on('click', function () {
                $('#addmodal').modal('show');
            });
        } );

        function modalField(modal,data) {
            modal.find('#id').val(data['id']);
            modal.find('#name').val(data['name']);
            modal.find('#intTextBox').val(data['contact']);
        }

        function clearModalField(modal) {
            modal.on('hidden.bs.modal', function () {
                modal.find('#id').empty();
                modal.find('#name').empty();
                modal.find('#intTextBox').empty();
            })
        }

        function view(id) {
            // alert(id);
            let modal = $('#view-modal');
            $.ajax({
                type: 'get',
                url: 'hotline/'+id,
                success: function (data) {
                    console.log(data);
                    // alert(data['location']);
                    modalField(modal,data);
                }
            });
            modal.modal('show');
            clearModalField(modal);
        }

        function del(id) {
            let modal = $('#delete-modal');
            $.ajax({
                type: 'get',
                url: 'hotline/'+id,
                success: function (data) {
                    modalField(modal,data);
                }
            });
            modal.modal('show');
            clearModalField(modal);
        }

        function edit(id) {
            // alert(id);
            let modal = $('#edit-modal');
            $.ajax({
                type: 'get',
                url: 'hotline/'+id,
                success: function (data) {
                    console.log(data);
                    // alert(data['location']);
                    modalField(modal,data);
                }
            });
            modal.modal('show');
            clearModalField(modal);
        }
    </script>
@stop
