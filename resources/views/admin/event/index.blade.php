@extends('admin.layout.app')

@section('content')

    <!-- BREADCRUMB-->
    {{--<section class="au-breadcrumb2">--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-md-12">--}}
                    {{--<div class="au-breadcrumb-content">--}}
                        {{--<div class="au-breadcrumb-left">--}}
                            {{--<span class="au-breadcrumb-span">You are here:</span>--}}
                            {{--<ul class="list-unstyled list-inline au-breadcrumb__list">--}}
                                {{--<li class="list-inline-item active">--}}
                                    {{--<a href="{{ route('admin.index') }}">Home</a>--}}
                                {{--</li>--}}
                                {{--<li class="list-inline-item seprate">--}}
                                    {{--<span>/</span>--}}
                                {{--</li>--}}
                                {{--<li class="list-inline-item">Events</li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</section>--}}
    <!-- END BREADCRUMB-->

    <!-- ALERT-->
    @if (session()->has('message'))
    <div class="alert au-alert-success alert-dismissible fade show au-alert au-alert--70per" role="alert">
        <i class="zmdi zmdi-check-circle"></i>
        <span class="content">{{ session()->get('message') }}</span>
        <button class="close" type="button" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">
                <i class="zmdi zmdi-close-circle"></i>
            </span>
        </button>
    </div>
    @endif
    <!-- END ALERT-->

    <!-- WELCOME-->
    <section class="welcome p-t-10">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="title-4">Events List
                        {{--<span>{{ Auth::user()->name }}</span>--}}
                    </h1>
                    <hr class="line-seprate">
                </div>
            </div>
        </div>
    </section>
    <!-- END WELCOME-->

    <!-- DATA TABLE-->
    <section class="p-t-20">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    {{--<h3 class="title-5 m-b-35">Events</h3>--}}
                    <div class="table-data__tool">
                        <div class="table-data__tool-right">
                            <button class="au-btn au-btn-icon au-btn--green au-btn--small" id="add-modal">
                                <i class="zmdi zmdi-plus"></i>Event
                            </button>
                        </div>
                    </div>
                    {{-- User accounts --}}
                    <div class="table-responsive table-responsive-data2">
                        <table class="table table-data2 table-striped table-bordered" id="eventTable">
                            <thead>
                            <tr>
                                <th>Event Name</th>
                                <th>Location</th>
                                <th>Date</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($events as $event)
                                <tr class="tr-shadow">
                                    <td class="title-5">{{ $event->name }}</td>
                                    <td>
                                        <span class="block-email">{{ $event->location }}</span>
                                    </td>
                                    <td>{{ date('M j, Y ',strtotime($event->date)) }}</td>
                                    <td>
                                        <div class="table-data-feature">
                                            <button class="item" data-toggle="tooltip" data-placement="top" title="View Details" onclick="view({{ $event->id }})">
                                                <i class="zmdi zmdi-more"></i>
                                            </button>
                                            <button class="item" data-toggle="tooltip" data-placement="top" title="Edit" onclick="edit({{ $event->id }})">
                                                <i class="zmdi zmdi-edit"></i>
                                            </button>
                                            <button class="item" data-toggle="tooltip" data-placement="top" title="Remove" onclick="del({{ $event->id }})">
                                                <i class="zmdi zmdi-delete"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END DATA TABLE-->

    <!-- add modal -->
    <div class="modal fade" id="addmodal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="scrollmodalLabel">Event Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('event.store') }}" method="post" class="form-horizontal">
                <div class="modal-body">
                    {{--CONTENT--}}
                        @csrf
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label for="name" class=" form-control-label">Event Name</label>
                            </div>
                            <div class="col-12 col-md-9">
                                <input type="text" id="name" name="name" placeholder="Event Name" class="form-control" required>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label for="location" class=" form-control-label">Location</label>
                            </div>
                            <div class="col-12 col-md-9">
                                <select class="form-control" name="location" required>
                                    <option value="">Select Location</option>
                                    @foreach ($locations as $location)
                                        <option value="{{ $location->name }}">{{ $location->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label for="date" class=" form-control-label">Dates</label>
                            </div>
                            <div class="col-12 col-md-9">
                                <input type="date" id="date" name="date" placeholder="Event Date" class="form-control" required>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label for="detail" class=" form-control-label">History / Details</label>
                            </div>
                            <div class="col-12 col-md-9">
                                <textarea name="detail" id="detail" rows="9" placeholder="Content..." class="form-control" required></textarea>
                            </div>
                        </div>

                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-danger btn-sm" data-dismiss="modal">
                        <i class="fa fa-ban"></i> Close
                    </button>
                    <button type="submit" class="btn btn-primary btn-sm">
                        <i class="fa fa-dot-circle-o"></i> Submit
                    </button>
                </div>
                </form>
            </div>
        </div>
    </div>
    <!-- end modal scroll -->

    <!-- view detail modal -->
    <div class="modal fade" id="detailmodal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="scrollmodalLabel">Event Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {{--CONTENT--}}
                    <div class="row">
                        <input type="hidden" id="id">
                        <h4 id="modal-title"></h4>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label><b>Event Name</b></label>
                                <input id="name" disabled class="form-control-plaintext">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label><b>Date</b></label>
                                <input id="date" disabled class="form-control-plaintext">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label><b>Location</b></label>
                                <input id="location" disabled class="form-control-plaintext">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label><b>Detail</b></label>
                                <textarea id="detail" rows="5" disabled class="form-control-plaintext"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-info btn-sm" data-dismiss="modal">
                        <i class="fa fa-ban"></i> Close
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- end view detail modal scroll -->

    <!-- edit modal -->
    <div class="modal fade" id="edit-modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Event</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                </div>
                <form method="post" action="{{ route('event.update', 0) }}">
                    @csrf
                    @method('put')
                    <div class="modal-body">
                        <input type="hidden" name="id" id="id">
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="name">Event Name</label>
                                    <input type="text" class="form-control" name="name" id="name" required>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="date">Date</label>
                                    <input type="date" class="form-control" id="date" name="date" required>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="location">Location</label>
                                    <select class="form-control" id="location" name="location" required>
                                        <option value="">Select Location</option>
                                        @foreach ($locations as $location)
                                            <option value="{{ $location->name }}">{{ $location->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="detail">Details</label>
                                    <textarea rows="5" id="detail" name="detail" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save Changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- end edit event -->

    <!-- delete modal -->
    <div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="staticModalLabel" aria-hidden="true"
         data-backdrop="static">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticModalLabel">Remove Event</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="d-inline-block" method="post" action="{{ route('event.destroy', 0) }}">
                    @csrf
                    @method('delete')
                    <div class="modal-body">
                        <input type="hidden" id="id" name="id">
                        <input type="text" class="form-control" id="name" disabled>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">
                            <i class="fa fa-ban"></i> Cancel
                        </button>
                        <button type="submit" class="btn btn-danger btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Delete
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- end delete modal -->

@endsection

@section('javascript')
    <script>
        $(document).ready( function () {
            $('#eventTable').DataTable();

            $('#add-modal').on('click', function () {
                $('#addmodal').modal('show');
            });

        } );

        function modalField(modal,data) {
            modal.find('#id').val(data['id']);
            modal.find('#name').val(data['name']);
            modal.find('#date').val(data['date']);
            modal.find('#location').val(data['location']);
            modal.find('#detail').append(data['detail']);
        }

        function clearModalField(modal) {
            modal.on('hidden.bs.modal', function () {
                modal.find('#id').empty();
                modal.find('#name').empty();
                modal.find('#date').empty();
                // modal.find('#location').empty();
                modal.find('#detail').empty();
            })
        }

        function view(id) {
            // alert(id);
            let modal = $('#detailmodal');
            $.ajax({
                type: 'get',
                url: 'event/'+id,
                success: function (data) {
                    console.log(data);
                    // alert(data['location']);
                    modalField(modal,data);
                }
            });
            modal.modal('show');
            clearModalField(modal);
        }

        function del(id) {
            let modal = $('#delete-modal');
            $.ajax({
                type: 'get',
                url: 'event/'+id,
                success: function (data) {
                    modalField(modal,data);
                }
            });
            modal.modal('show');
            clearModalField(modal);
        }

        function edit(id) {
            // alert(id);
            let modal = $('#edit-modal');
            $.ajax({
                type: 'get',
                url: 'event/'+id,
                success: function (data) {
                    console.log(data);
                    // alert(data['location']);
                    modalField(modal,data);
                }
            });
            modal.modal('show');
            clearModalField(modal);
        }

    </script>
@stop
