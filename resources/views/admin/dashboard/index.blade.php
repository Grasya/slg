@extends('admin.layout.app')

@section('content')

    <!-- WELCOME-->
    <section class="welcome p-t-10">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="title-4">Welcome back
                        <span>{{ Auth::user()->name }}</span>
                    </h1>
                    <hr class="line-seprate">
                </div>
            </div>
        </div>
    </section>
    <!-- END WELCOME-->

    <!-- STATISTIC-->
    <section class="statistic statistic2">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-3">
                    <div class="statistic__item statistic__item--green">
                        <h2 class="number">108</h2>
                        <span class="desc">Registered Owner</span>
                        <div class="icon">
                            <i class="zmdi zmdi-account-o"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="statistic__item statistic__item--orange">
                        <h2 class="number">88</h2>
                        <span class="desc">Resort</span>
                        <div class="icon">
                            <i class="zmdi zmdi-shopping-cart"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="statistic__item statistic__item--blue">
                        <h2 class="number">1,086</h2>
                        <span class="desc">Beach</span>
                        <div class="icon">
                            <i class="zmdi zmdi-calendar-note"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="statistic__item statistic__item--red">
                        <h2 class="number">386</h2>
                        <span class="desc">Pools</span>
                        <div class="icon">
                            <i class="zmdi zmdi-money"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END STATISTIC-->

    <!-- DATA TABLE-->
    <section class="p-t-20">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="title-5 m-b-35">Accounts</h3>
                    <div class="table-data__tool">
                        <div class="table-data__tool-right">
                            <button class="au-btn au-btn-icon au-btn--green au-btn--small" id="add-modal">
                                <i class="zmdi zmdi-plus"></i>Add Account
                            </button>
                        </div>
                    </div>
                    {{-- User accounts --}}
                    <div class="table-responsive table-responsive-data2">
                        <table class="table table-data2 table-striped table-bordered" id="accountTable">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Date Created</th>
                                <th>Role</th>
                                {{--<th></th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($users as $user)
                                <tr class="tr-shadow">
                                    <td class="title-5">{{ $user->name }}</td>
                                    <td>
                                        <span class="block-email">{{ $user->email }}</span>
                                    </td>
                                    <td>{{ date('h:i A M j, Y ',strtotime($user->created_at)) }}</td>
                                    <td class="title-5">
                                        <span class="role {{ $user->role != 'admin' ? 'member' : 'admin' }}">{{ $user->role }}</span>
                                    </td>
                                    {{--<td>--}}
                                        {{--<div class="table-data-feature">--}}
                                            {{--<button class="item" data-toggle="tooltip" data-placement="top" title="Send">--}}
                                                {{--<i class="zmdi zmdi-mail-send"></i>--}}
                                            {{--</button>--}}
                                            {{--<button class="item" data-toggle="tooltip" data-placement="top" title="Edit">--}}
                                                {{--<i class="zmdi zmdi-edit"></i>--}}
                                            {{--</button>--}}
                                            {{--<button class="item" data-toggle="tooltip" data-placement="top" title="Delete">--}}
                                                {{--<i class="zmdi zmdi-delete"></i>--}}
                                            {{--</button>--}}
                                            {{--<button class="item" data-toggle="tooltip" data-placement="top" title="More">--}}
                                                {{--<i class="zmdi zmdi-more"></i>--}}
                                            {{--</button>--}}
                                        {{--</div>--}}
                                    {{--</td>--}}
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END DATA TABLE-->

    <!-- add modal -->
    <div class="modal fade" id="addmodal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="scrollmodalLabel">Event Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('admin.store') }}" method="post" class="form-horizontal">
                    <div class="modal-body">
                        {{--CONTENT--}}
                        @csrf
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus autocomplete="off">

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="Role" class="col-md-4 col-form-label text-md-right">{{ __('Role') }}</label>

                            <div class="col-md-6">
                                <select id="role" class="form-control{{ $errors->has('role') ? ' is-invalid' : '' }}" name="role" value="{{ old('role') }}" required autocomplete="off">
                                    <option value="">Select Role</option>
                                    <option value="admin">CITO Admin</option>
                                    <option value="owner">Resort Owner</option>
                                </select>
                                @if ($errors->has('role'))
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('role') }}</strong>
                                        </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autocomplete="off">

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required autocomplete="off">

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="off">
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-danger btn-sm" data-dismiss="modal">
                            <i class="fa fa-ban"></i> Cancel
                        </button>
                        <button type="submit" class="btn btn-primary btn-sm">
                            <i class="fa fa-dot-circle-o"></i> {{ __('Register') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- end modal scroll -->

@endsection

@section('javascript')
    <script>
        $(document).ready( function () {
            $('#accountTable').DataTable();
            $('#add-modal').on('click', function () {
                $('#addmodal').modal('show');
            });
        } );
    </script>
@stop
