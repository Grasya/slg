<!DOCTYPE html>
<html lang="zxx" class="no-js">
<head>
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <link rel="shortcut icon" href="img/fav.png">
    <!-- Author Meta -->
    <meta name="author" content="CodePixar">
    <!-- Meta Description -->
    <meta name="description" content="">
    <!-- Meta Keyword -->
    <meta name="keywords" content="">
    <!-- meta character set -->
    <meta charset="UTF-8">
    <!-- Site Title -->
    <title>Dinomuz</title>

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,500,600" rel="stylesheet">
    <!--
    CSS
    ============================================= -->
    <link rel="stylesheet" href="{{ asset('landingpage/css/linearicons.css') }}">
    <link rel="stylesheet" href="{{ asset('landingpage/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('landingpage/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('landingpage/css/nice-select.css') }}">
    <link rel="stylesheet" href="{{ asset('landingpage/css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('landingpage/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('landingpage/css/main.css') }}">
</head>
<body>
<div class="main-wrapper-first">
    <header>
        <div class="container">
            <div class="header-wrap">
                <div class="header-top d-flex justify-content-between align-items-center">
                    <div class="logo">
                        {{--<a href="#"><img src="{{ asset('images/samal-logo.PNG') }}" alt=""></a>--}}
                    </div>
                    <div class="main-menubar d-flex align-items-center">
                        <nav class="hide">
                            <a href="#">Home</a>
                            <a href="#">Generic</a>
                            <a href="#">Elements</a>
                        </nav>
                        <div class="menu-bar"><span class="lnr lnr-menu"></span></div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="banner-area">
        <div class="container">
            <div class="row justify-content-center generic-height align-items-center">
                <div class="col-lg-8">
                    <div class="banner-content text-center">
                        <span class="text-white top text-uppercase">Re-imagining the way</span>
                        <h1 class="text-white text-uppercase">Generic Page</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Start Feature Area -->
    <section class="featured-area">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="single-feature d-flex flex-wrap justify-content-between">
                        <div class="icon d-flex align-items-center justify-content-center">
                            <span class="lnr lnr-list"></span>
                        </div>
                        <div class="desc">
                            <h6 class="title text-uppercase">Rates</h6>
                            <ul>
                                <li>1. Entrance - 100</li>
                                <li>2. Boat Fare - 50</li>
                                <li>3. Cottage - 100 Small </li>
                                <li>4. Cottage - 300 Medium </li>
                                <li>5. Cottage - 500 Large </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="single-feature d-flex flex-wrap justify-content-between">
                        <div class="icon d-flex align-items-center justify-content-center">
                            <span class="lnr lnr-code"></span>
                        </div>
                        <div class="desc">
                            <h6 class="title text-uppercase">Amenities</h6>
                            <ul>
                                <li>1. Cottage</li>
                                <li>2. Volleyball Court</li>
                                <li>3. Bar</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Feature Area -->

    <section class="amazing-works-area">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="section-title text-center">
                    <h3>Our Amazing Works</h3>
                    <span class="text-uppercase">Re-imagining the way</span>
                </div>
            </div>
        </div>
        <div class="active-works-carousel mt-40">
            <div class="item">
                <div class="thumb" style="background: url({{ asset('landingpage/img/w1.jpg') }});"></div>
                <div class="caption text-center">
                    <h6 class="text-uppercase">Vector Illustration</h6>
                    <p>LCD screens are uniquely modern in style, and the liquid crystals that make them work have allowed humanity to</p>
                </div>
            </div>
            <div class="item">
                <div class="thumb" style="background: url({{ asset('landingpage/img/w1.jpg') }});"></div>
                <div class="caption text-center">
                    <h6 class="text-uppercase">Vector Illustration</h6>
                    <p>LCD screens are uniquely modern in style, and the liquid crystals that make them work have allowed humanity to</p>
                </div>
            </div>
        </div>
    </section>

    <!-- Start Story Area -->
    <section class="story-area">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-3">
                    <div class="story-title">
                        <h3 class="text-white">Our Untold Story</h3>
                        <span class="text-uppercase text-white">Re-imagining the way</span>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="story-box">
                        <h6 class="text-uppercase">From the part of beginning</h6>
                        <p>Recently, the US Federal government banned online casinos from operating in America by making
                            it illegal to transfer money to them through any US bank or payment system. As a result of this
                            law, most of the popular online casino networks such as Party Gaming and PlayTech left the United
                            States.
                        </p>
                        <p>Overnight, online casino players found themselves being chased by the Federal government.
                            But, after a fortnight, the online casino industry came up with a solution and new online casinos
                            started taking root. These began to operate under a different business umbrella, and by doing that,
                            rendered the transfer of money to and from them legal. A major part of this was enlisting electronic
                            banking systems that would accept this new clarification and start doing business with me. Listed in
                            this article are the electronic banking systems that accept players from the United States that wish
                            to play in online casinos.</p>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Story Area -->
    <!-- Start Footer Widget Area -->
    <section class="footer-widget-area">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="single-widget d-flex flex-wrap justify-content-between">
                        <div class="icon d-flex align-items-center justify-content-center">
                            <span class="lnr lnr-pushpin"></span>
                        </div>
                        <div class="desc">
                            <h6 class="title text-uppercase">Address</h6>
                            <p>56/8, panthapath, west <br> dhanmondi, kalabagan, <br>Dhaka - 1205</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="single-widget d-flex flex-wrap justify-content-between">
                        <div class="icon d-flex align-items-center justify-content-center">
                            <span class="lnr lnr-earth"></span>
                        </div>
                        <div class="desc">
                            <h6 class="title text-uppercase">Email Address</h6>
                            <div class="contact">
                                <a href="mailto:info@dataarc.com">info@dataarc.com</a> <br>
                                <a href="mailto:support@dataarc.com">support@dataarc.com</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="single-widget d-flex flex-wrap justify-content-between">
                        <div class="icon d-flex align-items-center justify-content-center">
                            <span class="lnr lnr-phone"></span>
                        </div>
                        <div class="desc">
                            <h6 class="title text-uppercase">Phone Number</h6>
                            <div class="contact">
                                <a href="tel:1545">012 4562 982 3612</a> <br>
                                <a href="tel:54512">012 6321 956 4587</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Footer Widget Area -->
    <!-- Start footer Area -->
    <footer>
        <div class="container">
            <div class="footer-content d-flex justify-content-between align-items-center flex-wrap">
                <div class="logo">
                    <img src="img/logo.png" alt="">
                </div>
                <div class="copy-right-text">Copyright &copy; 2017  |  All rights reserved to Dinomuz inc. This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a></div>
                <div class="footer-social">
                    <a href="#"><i class="fa fa-facebook"></i></a>
                    <a href="#"><i class="fa fa-twitter"></i></a>
                    <a href="#"><i class="fa fa-dribbble"></i></a>
                    <a href="#"><i class="fa fa-behance"></i></a>
                </div>
            </div>
        </div>
    </footer>
    <!-- End footer Area -->




</div>




<script src="{{ asset('landingpage/js/vendor/jquery-2.2.4.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="{{ asset('landingpage/js/vendor/bootstrap.min.js') }}"></script>
<script src="{{ asset('landingpage/js/jquery.ajaxchimp.min.js') }}"></script>
<script src="{{ asset('landingpage/js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('landingpage/js/jquery.nice-select.min.js') }}"></script>
<script src="{{ asset('landingpage/js/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('landingpage/js/main.js') }}"></script>
</body>
</html>
