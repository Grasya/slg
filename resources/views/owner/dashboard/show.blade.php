@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col">
            @if (session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
        </div>
    </div>
    <div class="row mb-2">
        <div class="col-3">
            <h3>Rates & Amenities</h3>
        </div>
        <div class="col mt-1">
            <button class="btn btn-success btn-sm" id="addmodal">
                <i class="fa fa-plus"></i>

            </button>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Label</th>
                        <th>Amount</th>
                        <th>Category</th>
                        <th>Capacity</th>
                        <th>Hours</th>
                        <th>Size(S-M-L)</th>
                        <th>Features</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($rates as $rate)
                        <tr>
                            <td>{{ $rate->name }}</td>
                            <td>{{ $rate->amount }}</td>
                            <td>{{ $rate->category }}</td>
                            <td>{{ $rate->capacity }}</td>
                            <td>{{ $rate->hours }}</td>
                            <td>{{ $rate->size }}</td>
                            <td>{{ $rate->feature }}</td>
                            <td>
                                <button class="btn btn-sm btn-info" onclick="edit({{ $rate->id }})"><i class="fa fa-pencil-alt"></i></button>
                                <button class="btn btn-sm btn-danger" onclick="del({{ $rate->id }})"><i class="fa fa-minus"></i></button>

                                <!-- delete modal -->
                                <div class="modal fade" id="delete-modal{{ $rate->id }}">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title text-center">Delete Hotline</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span></button>
                                            </div>
                                            <div class="modal-body text-center">
                                                <h3>Are you sure you want to delete?</h3>
                                                <form class="d-inline-block" method="post" action="{{ route('rates.destroy', $rate->id) }}">
                                                    @csrf
                                                    @method('delete')
                                                    <input type="hidden" id="id" name="id">
                                                    <button type="submit" class="btn btn-danger">Delete</button>
                                                </form>
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- edit modal -->
                                <div class="modal fade" id="edit-modal{{ $rate->id }}">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">Edit detail</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span></button>
                                            </div>
                                            <form method="post" action="{{ route('rates.update', $rate->id) }}">
                                                @csrf
                                                @method('put')
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="form-group col">
                                                            <label for="category">Category</label>
                                                            <select class="form-control" id="category" name="category" required>
                                                                <option value="" {{ $rate->category == '' ? 'selected' : '' }}>Select Category</option>
                                                                <option value="rate" {{ $rate->category == 'rate' ? 'selected' : '' }}>Rates</option>
                                                                <option value="amenity" {{ $rate->category == 'amenity' ? 'selected' : '' }}>Amenities</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group col">
                                                            <label for="name">Rates/Amenities Label</label>
                                                            <input type="text" class="form-control" name="name" id="name" placeholder="Rates/Amenities Label" required autocomplete="false" value="{{ $rate->name }}">
                                                        </div>s
                                                    </div>

                                                    <div class="row">
                                                        <div class="form-group col">
                                                            <label for="size">Size</label>
                                                            <select class="form-control" id="size" name="size">
                                                                <option value="" {{ $rate->size == '' ? 'selected' : '' }}>Select Size</option>
                                                                <option value="Small" {{ $rate->size == 'Small' ? 'selected' : '' }}>Small</option>
                                                                <option value="Medium" {{ $rate->size == 'Medium' ? 'selected' : '' }}>Medium</option>
                                                                <option value="Large" {{ $rate->size == 'Large' ? 'selected' : '' }}>Large</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group col">
                                                            <label for="amount">Amount</label>
                                                            <input type="number" class="form-control" name="amount" id="amount" placeholder="Amount" autocomplete="false" value="{{ $rate->amount }}">
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="form-group col">
                                                            <label for="capacity">Capacity</label>
                                                            <input type="text" class="form-control" name="capacity" id="capacity" placeholder="Capacity" autocomplete="false" value="{{ $rate->capacity }}">
                                                        </div>
                                                        <div class="form-group col">
                                                            <label for="hours">Hours</label>
                                                            <input type="text" class="form-control" name="hours" id="hours" placeholder="Hours" autocomplete="false" value="{{ $rate->hours }}">
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col">
                                                            <label for="feature">Feature</label>
                                                            <textarea class="form-control" name="feature" id="feature" rows="5" autocomplete="false">
                                                                {{ $rate->feature }}
                                                            </textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-success">Save Changes</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $rates->links() }}

        </div>
    </div>

    <!-- add modal -->
    <div class="modal fade" id="add-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add detail</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                </div>
                <form method="post" action="{{ route('rates.store') }}">
                    @csrf
                    <div class="modal-body">
                        <div class="row">
                            <div class="form-group col">
                                <label for="category">Category</label>
                                <select class="form-control" id="category" name="category" required>
                                    <option value="">Select Category</option>
                                    <option value="rate">Rates</option>
                                    <option value="amenity">Amenities</option>
                                </select>
                            </div>
                            <div class="form-group col">
                                <label for="name">Rates/Amenities Label</label>
                                <input type="text" class="form-control" name="name" id="name" placeholder="Rates/Amenities Label" required autocomplete="false">
                            </div>s
                        </div>

                        <div class="row">
                            <div class="form-group col">
                                <label for="size">Size</label>
                                <select class="form-control" id="size" name="size">
                                    <option value="">Select Size</option>
                                    <option value="Small">Small</option>
                                    <option value="Medium">Medium</option>
                                    <option value="Large">Large</option>
                                </select>
                            </div>
                            <div class="form-group col">
                                <label for="amount">Amount</label>
                                <input type="number" class="form-control" name="amount" id="amount" placeholder="Amount" autocomplete="false">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col">
                                <label for="capacity">Capacity</label>
                                <input type="text" class="form-control" name="capacity" id="capacity" placeholder="Capacity" autocomplete="false">
                            </div>
                            <div class="form-group col">
                                <label for="hours">Hours</label>
                                <input type="text" class="form-control" name="hours" id="hours" placeholder="Hours" autocomplete="false">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="feature">Feature</label>
                                <textarea class="form-control" name="feature" id="feature" rows="5" autocomplete="false">
                                </textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


@stop

@section('script')
    <script>
        $('#addmodal').on('click', function () {
            $('#add-modal').modal('show');
        });
        function edit(id) {
            $('#edit-modal'+id).modal('show');
        }

        function del(id) {
            $('#delete-modal'+id).modal('show');
        }
    </script>
@stop
