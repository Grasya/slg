@extends('owner.layout.app')

@section('content')

    <!-- STATISTIC-->
    <section class="statistic statistic2">
        <div class="container">
            @if ($resort)
                <div class="row">
                    <div class="col-3">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title mb-3">Settings</strong>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        Page Info
                                    </div>
                                    <div class="col-4">
                                        <button class="btn btn-secondary btn-sm pull-right" id="btn_pageinfo">
                                            <i class="fa fa-cog"></i>
                                        </button>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col">
                                        Amenities & Rates
                                    </div>
                                    <div class="col-4">
                                        <button class="btn btn-secondary btn-sm pull-right" id="btn_amenity">
                                            <i class="fa fa-cog"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Page info -->

                    <div class="col">
                        <form action="{{ route('owner.update', $resort[0]['id']) }}">
                            @csrf
                            @method('put')
                            <div class="card " id="card_pageinfo">
                                <div class="card-header">
                                    <strong class="card-title mb-3">
                                        <i class="fa fa-cog"></i> Page Info
                                    </strong>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col">
                                            <h5>GENERAL</h5>
                                            <hr>
                                            <div class="row">
                                                <div class="col-2">
                                                    <label>Title</label>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group">
                                                        @foreach ($resort[0]['amenity'] as $ame)
                                                            <input hidden value="{{ $ame }}" name="amenity[]">
                                                        @endforeach
                                                        <input hidden value="{{ $resort[0]['id'] }}" name="id">
                                                        <input type="text" class="form-control" name="title" value="{{ $resort[0]['title'] }}" placeholder="Title">
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row">
                                                <div class="col-2">
                                                    <label>Description</label>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group">
                                                        <textarea class="form-control" rows="5" placeholder="Description" name="description">{{ $resort[0]['description'] }}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row">
                                                <div class="col-2">
                                                    <label>Category</label>
                                                </div>
                                                <div class="col">
                                                    <form action="" method="post">
                                                        <div class="form-group">
                                                            <div class="checkbox">
                                                                <label for="category1" class="form-check-label ">
                                                                    <input type="checkbox" id="category1" name="category[]" value="Beach"
                                                                           @foreach ($resort[0]['category'] as $cat)
                                                                               {{ $cat == 'Beach' ? 'checked' : '' }}
                                                                           @endforeach
                                                                           class="form-check-input"> Beach
                                                                </label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label for="category2" class="form-check-label ">
                                                                    <input type="checkbox" id="category2" name="category[]" value="Pool"
                                                                           @foreach ($resort[0]['category'] as $cat)
                                                                                {{ $cat == 'Pool' ? 'checked' : '' }}
                                                                           @endforeach
                                                                           class="form-check-input"> Pool
                                                                </label>
                                                            </div>
                                                        </div>
                                                        {{--<button class="btn btn-success btn-sm" type="submit">Save Changes</button>--}}
                                                        {{--<a href="#form_title" class="">Cancel</a>--}}
                                                    </form>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row">
                                                <div class="col-2">
                                                    <label>Location</label>
                                                </div>
                                                <div class="col">
                                                    <select class="form-control" name="location">
                                                        <option>Select location</option>
                                                        @foreach ($locations as $location)
                                                            <option value="{{ $location->name }}" {{ $resort[0]['location'] === $location->name ? 'selected' : '' }}>{{ $location->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row">
                                                <div class="col-2">
                                                    {{--<label>Image</label>--}}
                                                </div>
                                                <div class="col">
                                                    {{--<div class="form-group">--}}
                                                    {{--<input type="file" class="form-control" name="images[]" multiple/>--}}
                                                    {{--<input type="hidden" name="resort_id" value=""/>--}}
                                                    {{--</div>--}}

                                                    {{--<a href="#form_image" class="">Cancel</a>--}}
                                                </div>
                                            </div>
                                            <button class="btn btn-success btn-sm" type="submit">Save Changes</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                            <!-- end page info -->

                            <!-- Amenities -->
                            <div class="card " id="card_amenity">
                                <div class="card-header">
                                    <strong class="card-title mb-3">
                                        <i class="fa fa-cog"></i> Amenities & Rates
                                    </strong>
                                </div>
                                <div class="card-body">
                                    <form action="{{ route('owner.update', $resort[0]['id']) }}">
                                        @csrf
                                        @method('put')
                                        <input hidden value="{{ $resort[0]['title'] }}" name="title">
                                        <input hidden value="{{ $resort[0]['id'] }}" name="id">
                                        <input hidden value="{{ $resort[0]['location'] }}" name="location">
                                        <input hidden value="{{ $resort[0]['description'] }}" name="description">
                                        @foreach ($resort[0]['category'] as $cat)
                                            <input hidden value="{{ $cat }}" name="category[]">
                                        @endforeach
                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label class=" form-control-label">Check Available</label>
                                        </div>
                                        <div class="col col-md-4">
                                            <div class="form-check">
                                                <div class="checkbox">
                                                    <label for="amenity1" class="form-check-label ">
                                                        <input type="checkbox" id="amenity1" name="amenity[]"
                                                               @foreach ($resort[0]['amenity'] as $amenity)
                                                                    {{ $amenity == 'Aircon' ? 'checked' : '' }}
                                                               @endforeach
                                                               value="Aircon" class="form-check-input"> Aircon
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label for="amenity2" class="form-check-label ">
                                                        <input type="checkbox" id="amenity2" name="amenity[]"
                                                               @foreach ($resort[0]['amenity'] as $amenity)
                                                               {{ $amenity == 'Non Aircon' ? 'checked' : '' }}
                                                               @endforeach
                                                               value="Non Aircon" class="form-check-input"> Non Aircon
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label for="amenity3" class="form-check-label ">
                                                        <input type="checkbox" id="amenity3" name="amenity[]"
                                                               @foreach ($resort[0]['amenity'] as $amenity)
                                                               {{ $amenity == 'Cottage' ? 'checked' : '' }}
                                                               @endforeach
                                                               value="Cottage" class="form-check-input"> Cottage
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label for="amenity4" class="form-check-label ">
                                                        <input type="checkbox" id="amenity4" name="amenity[]"
                                                               @foreach ($resort[0]['amenity'] as $amenity)
                                                               {{ $amenity == 'Rooms' ? 'checked' : '' }}
                                                               @endforeach
                                                               value="Rooms" class="form-check-input"> Rooms
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label for="amenity5" class="form-check-label ">
                                                        <input type="checkbox" id="amenity5" name="amenity[]"
                                                               @foreach ($resort[0]['amenity'] as $amenity)
                                                               {{ $amenity == 'Function Hall' ? 'checked' : '' }}
                                                               @endforeach
                                                               value="Function Hall" class="form-check-input"> Function Hall
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label for="amenity6" class="form-check-label ">
                                                        <input type="checkbox" id="amenity6" name="amenity[]"
                                                               @foreach ($resort[0]['amenity'] as $amenity)
                                                               {{ $amenity == 'Comfort Room' ? 'checked' : '' }}
                                                               @endforeach
                                                               value="Comfort Room" class="form-check-input"> Comfort Room
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label for="amenity7" class="form-check-label ">
                                                        <input type="checkbox" id="amenity7" name="amenity[]"
                                                               @foreach ($resort[0]['amenity'] as $amenity)
                                                               {{ $amenity == 'Restaurant' ? 'checked' : '' }}
                                                               @endforeach
                                                               value="Restaurant" class="form-check-input"> Restaurant
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label for="amenity8" class="form-check-label ">
                                                        <input type="checkbox" id="amenity8" name="amenity[]"
                                                               @foreach ($resort[0]['amenity'] as $amenity)
                                                               {{ $amenity == 'Bar' ? 'checked' : '' }}
                                                               @endforeach
                                                               value="Bar" class="form-check-input"> Bar
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label for="amenity9" class="form-check-label ">
                                                        <input type="checkbox" id="amenity9" name="amenity[]"
                                                               @foreach ($resort[0]['amenity'] as $amenity)
                                                               {{ $amenity == 'Spa' ? 'checked' : '' }}
                                                               @endforeach
                                                               value="Spa" class="form-check-input"> Spa
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label for="amenity10" class="form-check-label ">
                                                        <input type="checkbox" id="amenity10" name="amenity[]"
                                                               @foreach ($resort[0]['amenity'] as $amenity)
                                                               {{ $amenity == 'Gym' ? 'checked' : '' }}
                                                               @endforeach
                                                               value="Gym" class="form-check-input"> Gym
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label for="amenity11" class="form-check-label ">
                                                        <input type="checkbox" id="amenity11" name="amenity[]"
                                                               @foreach ($resort[0]['amenity'] as $amenity)
                                                               {{ $amenity == 'Basketball Court' ? 'checked' : '' }}
                                                               @endforeach
                                                               value="Basketball Court" class="form-check-input"> Basketball Court
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- second row-->
                                        <div class="col col-md-4">
                                            <div class="form-check">
                                                <div class="checkbox">
                                                    <label for="amenity12" class="form-check-label ">
                                                        <input type="checkbox" id="amenity12" name="amenity[]"
                                                               @foreach ($resort[0]['amenity'] as $amenity)
                                                               {{ $amenity == 'Beach Volleyball' ? 'checked' : '' }}
                                                               @endforeach
                                                               value="Beach Volleyball" class="form-check-input"> Beach Volleyball
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label for="amenity13" class="form-check-label ">
                                                        <input type="checkbox" id="amenity13" name="amenity[]"
                                                               @foreach ($resort[0]['amenity'] as $amenity)
                                                               {{ $amenity == 'Ping Pong' ? 'checked' : '' }}
                                                               @endforeach
                                                               value="Ping Pong" class="form-check-input"> Ping Pong
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label for="amenity14" class="form-check-label ">
                                                        <input type="checkbox" id="amenity14" name="amenity[]"
                                                               @foreach ($resort[0]['amenity'] as $amenity)
                                                               {{ $amenity == 'Air Hockey' ? 'checked' : '' }}
                                                               @endforeach
                                                               value="Air Hockey" class="form-check-input"> Air Hockey
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label for="amenity15" class="form-check-label ">
                                                        <input type="checkbox" id="amenity15" name="amenity[]"
                                                               @foreach ($resort[0]['amenity'] as $amenity)
                                                               {{ $amenity == 'Childrens Playground' ? 'checked' : '' }}
                                                               @endforeach
                                                               value="Childrens Playground" class="form-check-input"> Children's Playground
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label for="amenity16" class="form-check-label ">
                                                        <input type="checkbox" id="amenity16" name="amenity[]"
                                                               @foreach ($resort[0]['amenity'] as $amenity)
                                                               {{ $amenity == 'Aviary' ? 'checked' : '' }}
                                                               @endforeach
                                                               value="Aviary" class="form-check-input"> Aviary
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label for="amenity17" class="form-check-label ">
                                                        <input type="checkbox" id="amenity17" name="amenity[]"
                                                               @foreach ($resort[0]['amenity'] as $amenity)
                                                               {{ $amenity == 'Free Wifi' ? 'checked' : '' }}
                                                               @endforeach
                                                               value="Free Wifi" class="form-check-input"> Free Wifi
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label for="amenity18" class="form-check-label ">
                                                        <input type="checkbox" id="amenity18" name="amenity[]"
                                                               @foreach ($resort[0]['amenity'] as $amenity)
                                                               {{ $amenity == 'Camping Ground' ? 'checked' : '' }}
                                                               @endforeach
                                                               value="Camping Ground" class="form-check-input"> Camping Ground
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label for="amenity19" class="form-check-label ">
                                                        <input type="checkbox" id="amenity19" name="amenity[]"
                                                               @foreach ($resort[0]['amenity'] as $amenity)
                                                               {{ $amenity == 'Arcade' ? 'checked' : '' }}
                                                               @endforeach
                                                               value="Arcade" class="form-check-input"> Arcade
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label for="amenity20" class="form-check-label ">
                                                        <input type="checkbox" id="amenity20" name="amenity[]"
                                                               @foreach ($resort[0]['amenity'] as $amenity)
                                                               {{ $amenity == 'Board Room' ? 'checked' : '' }}
                                                               @endforeach
                                                               value="Board Room" class="form-check-input"> Board Room
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label for="amenity21" class="form-check-label ">
                                                        <input type="checkbox" id="amenity21" name="amenity[]"
                                                               @foreach ($resort[0]['amenity'] as $amenity)
                                                               {{ $amenity == 'Pavilion' ? 'checked' : '' }}
                                                               @endforeach
                                                               value="Pavilion" class="form-check-input"> Pavilion
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label for="amenity22" class="form-check-label ">
                                                        <input type="checkbox" id="amenity22" name="amenity[]"
                                                               @foreach ($resort[0]['amenity'] as $amenity)
                                                               {{ $amenity == 'Security' ? 'checked' : '' }}
                                                               @endforeach
                                                               value="Security" class="form-check-input"> 24 Hours Security
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <button class="btn btn-success btn-sm" type="submit">Save Changes</button>
                                    </form>
                                </div>
                            </div>
                        <!-- end amenity -->
                    </div>
                </div>
            @else
                <div class="row">
                    <div class="col-3">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title mb-3">Settings</strong>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        Page Info
                                    </div>
                                    <div class="col-4">
                                        <button class="btn btn-secondary btn-sm pull-right" id="btn_pageinfo">
                                            <i class="fa fa-cog"></i>
                                        </button>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col">
                                        Amenities & Rates
                                    </div>
                                    <div class="col-4">
                                        <button class="btn btn-secondary btn-sm pull-right" disabled id="btn_amenity">
                                            <i class="fa fa-cog"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Page info -->
                    <div class="col">
                        <div class="card " id="card_pageinfo">
                            <div class="card-header">
                                <strong class="card-title mb-3">
                                    <i class="fa fa-cog"></i> Page Info
                                </strong>
                            </div>
                            <form action="{{ route('owner.store') }}" method="POST">
                                @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5>GENERAL</h5>
                                        <hr>
                                        <div class="row">
                                            <div class="col-2">
                                                <label>Title</label>
                                            </div>
                                            <div class="col">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="title" placeholder="Title" required>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-2">
                                                <label>Location</label>
                                            </div>
                                            <div class="col">
                                                <div class="form-group">
                                                    <select class="form-control" name="location" required>
                                                        <option value="">Select Location</option>
                                                        @foreach ($locations as $location)
                                                            <option value="{{ $location->name }}">{{ $location->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-2">
                                                <label>Description</label>
                                            </div>
                                            <div class="col">
                                                <div class="form-group">
                                                    <textarea class="form-control" rows="5" placeholder="Description" name="description" required></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-2">
                                                <label>Category</label>
                                            </div>
                                            <div class="col">
                                                <form action="" method="post">
                                                    <div class="form-group">
                                                        <div class="checkbox">
                                                            <label for="category1" class="form-check-label ">
                                                                <input type="checkbox" id="category1" name="category[]" value="Beach" class="form-check-input"> Beach
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label for="category2" class="form-check-label ">
                                                                <input type="checkbox" id="category2" name="category[]" value="Pool" class="form-check-input"> Pool
                                                            </label>
                                                        </div>
                                                    </div>
                                                    {{--<button class="btn btn-success btn-sm" type="submit">Save Changes</button>--}}
                                                    {{--<a href="#form_title" class="">Cancel</a>--}}
                                                </form>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-2">
                                                <label>Location</label>
                                            </div>
                                            <div class="col">
                                                <select class="form-control" name="location">
                                                    <option>Select location</option>
                                                    @foreach ($locations as $location)
                                                        <option value="{{ $location->name }}">{{ $location->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-2">
                                                {{--<label>Image</label>--}}
                                            </div>
                                            <div class="col">
                                                {{--<div class="form-group">--}}
                                                {{--<input type="file" class="form-control" name="images[]" multiple/>--}}
                                                {{--<input type="hidden" name="resort_id" value=""/>--}}
                                                {{--</div>--}}

                                                {{--<a href="#form_image" class="">Cancel</a>--}}
                                            </div>
                                        </div>
                                        <button class="btn btn-success btn-sm" type="submit">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                            </form>
                        </div>
                        <!-- end page info -->

                        <!-- Amenities -->
                        <div class="card " id="card_amenity">
                            <div class="card-header">
                                <strong class="card-title mb-3">
                                    <i class="fa fa-cog"></i> Amenities & Rates
                                </strong>
                            </div>
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label class=" form-control-label">Check Available</label>
                                    </div>
                                    <div class="col col-md-4">
                                        <div class="form-check">
                                            <div class="checkbox">
                                                <label for="amenity1" class="form-check-label ">
                                                    <input type="checkbox" id="amenity1" name="amenity[]" value="Aircon" class="form-check-input"> Aircon
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label for="amenity2" class="form-check-label ">
                                                    <input type="checkbox" id="amenity2" name="amenity[]" value="Non Aircon" class="form-check-input"> Non Aircon
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label for="amenity3" class="form-check-label ">
                                                    <input type="checkbox" id="amenity3" name="amenity[]" value="Cottage" class="form-check-input"> Cottage
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label for="amenity4" class="form-check-label ">
                                                    <input type="checkbox" id="amenity4" name="amenity[]" value="Rooms" class="form-check-input"> Rooms
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label for="amenity5" class="form-check-label ">
                                                    <input type="checkbox" id="amenity5" name="amenity[]" value="Function Hall" class="form-check-input"> Function Hall
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label for="amenity6" class="form-check-label ">
                                                    <input type="checkbox" id="amenity6" name="amenity[]" value="Comfort Room" class="form-check-input"> Comfort Room
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label for="amenity7" class="form-check-label ">
                                                    <input type="checkbox" id="amenity7" name="amenity[]" value="Restaurant" class="form-check-input"> Restaurant
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label for="amenity8" class="form-check-label ">
                                                    <input type="checkbox" id="amenity8" name="amenity[]" value="Bar" class="form-check-input"> Bar
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label for="amenity9" class="form-check-label ">
                                                    <input type="checkbox" id="amenity9" name="amenity[]" value="Spa" class="form-check-input"> Spa
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label for="amenity10" class="form-check-label ">
                                                    <input type="checkbox" id="amenity10" name="amenity[]" value="Gym" class="form-check-input"> Gym
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label for="amenity11" class="form-check-label ">
                                                    <input type="checkbox" id="amenity11" name="amenity[]" value="Basketball Court" class="form-check-input"> Basketball Court
                                                </label>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col col-md-4">
                                        <div class="form-check">
                                            <div class="checkbox">
                                                <label for="amenity12" class="form-check-label ">
                                                    <input type="checkbox" id="amenity12" name="amenity[]" value="Beach Volleyball" class="form-check-input"> Beach Volleyball
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label for="amenity13" class="form-check-label ">
                                                    <input type="checkbox" id="amenity13" name="amenity[]" value="Ping Pong" class="form-check-input"> Ping Pong
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label for="amenity14" class="form-check-label ">
                                                    <input type="checkbox" id="amenity14" name="amenity[]" value="Air Hockey" class="form-check-input"> Air Hockey
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label for="amenity15" class="form-check-label ">
                                                    <input type="checkbox" id="amenity15" name="amenity[]" value="Childrens Playground" class="form-check-input"> Children's Playground
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label for="amenity16" class="form-check-label ">
                                                    <input type="checkbox" id="amenity16" name="amenity[]" value="Aviary" class="form-check-input"> Aviary
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label for="amenity17" class="form-check-label ">
                                                    <input type="checkbox" id="amenity17" name="amenity[]" value="Free Wifi" class="form-check-input"> Free Wifi
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label for="amenity18" class="form-check-label ">
                                                    <input type="checkbox" id="amenity18" name="amenity[]" value="Camping Ground" class="form-check-input"> Camping Ground
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label for="amenity19" class="form-check-label ">
                                                    <input type="checkbox" id="amenity19" name="amenity[]" value="Arcade" class="form-check-input"> Arcade
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label for="amenity20" class="form-check-label ">
                                                    <input type="checkbox" id="amenity20" name="amenity[]" value="Board Room" class="form-check-input"> Board Room
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label for="amenity21" class="form-check-label ">
                                                    <input type="checkbox" id="amenity21" name="amenity[]" value="Pavilion" class="form-check-input"> Pavilion
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label for="amenity22" class="form-check-label ">
                                                    <input type="checkbox" id="amenity22" name="amenity[]" value="Security" class="form-check-input"> 24 Hours Security
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <button class="btn btn-success btn-sm" type="submit">Save Changes</button>
                            </div>
                        </div>
                        <!-- end amenity -->
                    </div>
                </div>
            @endif
        </div>
    </section>
    <!-- END STATISTIC-->


@endsection

@section('javascript')
    <script>
        $(document).ready( function () {
            $('#accountTable').DataTable();
        } );
        let page = $('#card_pageinfo');
        let rate = $('#card_rates');
        let amenity = $('#card_amenity');

        showSettings();

        function showSettings() {
            $(amenity).addClass('hide-card');
        }

        $("#btn_pageinfo").on('click', function () {
            $(page).removeClass('hide-card');//toggle this
            $(amenity).addClass('hide-card');
        });

        $("#btn_amenity").on('click', function () {
            $(page).addClass('hide-card');
            $(amenity).removeClass('hide-card');//add this
        });

        function modalField(modal, data) {

            modal.find('#id').val(data['id']);
            modal.find('#name').val(data['name']);
            modal.find('#location').val(data['location']);
            let category = JSON.parse(data['category']);
            console.log(category[1]);
            for(let i=0; i<category.length; i++) {
                if(category[i] === 'Cave') {
                    modal.find('#category1').prop('checked', true);
                }
                if(category[i] === 'Mountain') {
                    modal.find('#category2').prop('checked', true);
                }
                if(category[i] === 'Diving') {
                    modal.find('#category3').prop('checked', true);
                }
                if(category[i] === 'Falls') {
                    modal.find('#category4').prop('checked', true);
                }
            }
        }

        function clearModalField(modal) {
            modal.on('hidden.bs.modal', function () {
                modal.find('#id').empty();
                modal.find('#name').empty();
                // modal.find('#location').empty();
                modal.find('.category').prop('checked', false);
            })
        }

        function view(id) {
            // alert(id);
            let modal = $('#view-modal');
            $.ajax({
                type: 'get',
                url: 'tourist/'+id,
                success: function (data) {
                    console.log(data);
                    // alert(data['location']);
                    modalField(modal,data);
                }
            });
            modal.modal('show');
            clearModalField(modal);
        }

        function del(id) {
            let modal = $('#deletemodal');
            $.ajax({
                type: 'get',
                url: 'tourist/'+id,
                success: function (data) {
                    modalField(modal,data);
                }
            });
            modal.modal('show');
            clearModalField(modal);
        }

        function edit(id) {
            // alert(id);
            let modal = $('#editmodal');
            $.ajax({
                type: 'get',
                url: 'tourist/'+id,
                success: function (data) {
                    console.log(data);
                    // alert(data['location']);
                    modalField(modal,data);
                }
            });
            modal.modal('show');
            clearModalField(modal);
        }
    </script>
@stop
