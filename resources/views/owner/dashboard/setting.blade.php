@extends('owner.layout.app')

@section('content')

    <!-- STATISTIC-->
    <section class="statistic statistic2">
        <div class="container">
            <div class="row">
                <div class="col-3">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title mb-3">Settings</strong>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    Page Info
                                </div>
                                <div class="col-4">
                                    <button class="btn btn-secondary btn-sm pull-right" id="btn_pageinfo">
                                        <i class="fa fa-cog"></i>
                                    </button>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col">
                                    Amenities
                                </div>
                                <div class="col-4">
                                    <button class="btn btn-secondary btn-sm pull-right" id="btn_amenities">
                                        <i class="fa fa-cog"></i>
                                    </button>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col">
                                    Rates
                                </div>
                                <div class="col-4">
                                    <button class="btn btn-secondary btn-sm pull-right" id="btn_rates">
                                        <i class="fa fa-cog"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Page info -->
                <div class="col">
                    <div class="card " id="card_pageinfo">
                        <div class="card-header">
                            <strong class="card-title mb-3">
                                <i class="fa fa-cog"></i> Page Info
                            </strong>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h5>GENERAL</h5>
                                    <hr>
                                    <div class="row">
                                        <div class="col-2">
                                            <label>Title</label>
                                        </div>
                                        <div class="col">
                                            <form action="" method="post" id="form_title">
                                                @csrf
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="title" placeholder="Title">
                                                </div>
                                                <button class="btn btn-success btn-sm" type="submit">Save Changes</button>
                                                <a href="#form_title" class="">Cancel</a>
                                            </form>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-2">
                                            <label>Description</label>
                                        </div>
                                        <div class="col">
                                            <form action="" method="post" id="form_desc">
                                                @csrf
                                                <div class="form-group">
                                                    <textarea class="form-control" rows="5" placeholder="Description" name="description"></textarea>
                                                </div>
                                                <button class="btn btn-success btn-sm" type="submit">Save Changes</button>
                                                <a href="#form_desc" class="">Cancel</a>
                                            </form>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-2">
                                            <label>Category</label>
                                        </div>
                                        <div class="col">
                                            <form action="" method="post">
                                                <div class="form-group">
                                                    <div class="checkbox">
                                                        <label for="category1" class="form-check-label ">
                                                            <input type="checkbox" id="category1" name="category[]" value="Beach" class="form-check-input"> Beach
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label for="category2" class="form-check-label ">
                                                            <input type="checkbox" id="category2" name="category[]" value="Pool" class="form-check-input"> Pool
                                                        </label>
                                                    </div>
                                                </div>
                                                {{--<button class="btn btn-success btn-sm" type="submit">Save Changes</button>--}}
                                                {{--<a href="#form_title" class="">Cancel</a>--}}
                                            </form>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-2">
                                            <label>Image</label>
                                        </div>
                                        <div class="col">
                                            <form method="post" action="{{ route('upload.store') }}" enctype="multipart/form-data" id="form_image">
                                                @csrf
                                                <div class="form-group">
                                                    <input type="file" class="form-control" name="images[]" multiple/>
                                                    <input type="hidden" name="resort_id" value=""/>
                                                </div>
                                                <button class="btn btn-success btn-sm" type="submit">Save Changes</button>
                                                <a href="#form_image" class="">Cancel</a>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end page info -->

                    <!-- Amenities -->
                    <div class="card " id="card_amenities">
                        <div class="card-header">
                            <strong class="card-title mb-3">
                                <i class="fa fa-cog"></i> Amenities
                            </strong>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col">

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end amenities -->

                    <!-- Rates -->
                    <div class="card " id="card_rates">
                        <div class="card-header">
                            <strong class="card-title mb-3">
                                <i class="fa fa-cog"></i> Rates
                            </strong>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col">

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end Rates -->


                </div>
            </div>
        </div>
    </section>
    <!-- END STATISTIC-->


@endsection

@section('javascript')
    <script>
        $(document).ready( function () {
            $('#accountTable').DataTable();
        } );
        let page = $('#card_pageinfo');
        let rate = $('#card_rates');
        let amenity = $('#card_amenities');

        function showSettings() {

        }

        $("#btn_pageinfo").on('click', function () {
            $(page).removeClass('hide-card');//toggle this
            $(amenity).addClass('hide-card');
            $(rate).addClass('hide-card');
        });

        $("#btn_amenities").on('click', function () {
            $(page).addClass('hide-card');
            $(amenity).removeClass('hide-card');//add this
            $(rate).addClass('hide-card');
        });

        $("#btn_rates").on('click', function () {
            $(page).addClass('hide-card');
            $(amenity).addClass('hide-card');
            $(rate).removeClass('hide-card');//add this
        });
    </script>
@stop
