@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col">
            <div class="card">
                
                <div class="card-body">
                    <img src="{{asset('avatar.png')}}" width="100%" style="border-radius: 50%" alt="avatar">
                    <p class="text-center text-capitalize mt-5">
                        {{ Auth::user()->name }} | {{ Auth::user()->role }}
                    </p>
                </div>
            </div>
        </div>
        <div class="col-8">
            <div class="card">
                <div class="card-header">
                    Profile Details
                </div>
                <div class="card-body">
                    <form action="{{ route('profile.update', $users->id ) }}" method="post" id="profile-update">
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input id="name" name="name" type="text" class="form-control" disabled="" value="{{ Auth::user()->name }}">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="role">Role</label>
                                    <select class="form-control" id="role" name="role" disabled="">
                                        <option value="owner" {{ Auth::user()->role() == 'owner' ? 'selected' : ''  }}>Owner</option>
                                        <option value="admin" {{ Auth::user()->role() == 'admin' ? 'selected' : ''  }}>Admin</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="email">E-mail</label>
                                    <input id="email" type="email" name="email" class="form-control" disabled="" value="{{ Auth::user()->email }}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <button class="btn btn-info" type="button" id="edit">Edit</button>
                                <button class="btn btn-success" type="submit" id="save" hidden>Save changes</button>
                                <button class="btn btn-default" type="button" id="cancel" hidden>Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    <script>
        let form = $('#profile-update');

        $('#edit').on('click', function () {
            form.find('.form-control').prop('disabled', false)
            form.find('#edit').prop('hidden', true)
            form.find('#save').prop('hidden', false)
            form.find('#cancel').prop('hidden', false)
        });

        $('#cancel').on('click', function () {
            form.find('.form-control').prop('disabled', true)
            form.find('#edit').prop('hidden', false)
            form.find('#save').prop('hidden', true)
            form.find('#cancel').prop('hidden', true)
        })
    </script>
@stop
