<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if(Auth::check()) {
        if(Auth::user()->role() == 'admin') {
            return redirect(route('admin.index'));
        }
        else if(Auth::user()->role() == 'owner') {
            return redirect(route('owner.index'));
        }
    }
    return view('auth.login');
});

Auth::routes();


Route::group(['middleware' => ['auth']], function () {
    Route::resource('profile', 'ProfileController');
});

Route::group(['middleware' => ['auth', 'admin']], function () {
//    Route::resource('admin', 'AdminController');
    Route::resource('admin', 'AdminDashboardController');
    Route::resource('event', 'EventController');
    Route::resource('tourist', 'TouristController');
    Route::resource('hotline', 'HotlineController');
    Route::resource('resort', 'ResortController');
    Route::resource('archive', 'ArchiveController');
});

Route::group(['middleware' => ['auth', 'owner']], function () {
    Route::resource('owner', 'OwnerController');
    Route::resource('rates', 'RateController');
//    Route::resource('resort', 'ResortController');
    Route::resource('upload', 'ImageController');
});

Route::resource('view', 'ViewController');
